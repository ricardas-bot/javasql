package sqljava;

import sqljava.entities.Employee;
import sqljava.services.EmployeeService;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Mysql {

    public static void main(String[] args) {
//        insertEmp(2, LocalDate.of(1960, 5, 12), "Alas", "Balas", "M", LocalDate.of(2015, 5, 15));
//        updateEmp( LocalDate.of(1960, 5, 12), "DSAlas", "Balas", "M", LocalDate.of(2015, 5, 15), 2);
//        removeEmp(2);
//        getEmp();

//        EmployeeService employeeService = new EmployeeService();
//        test(employeeService);
//
//        employeeService.setUseConnectionPool(true);
//        test(employeeService);

        EmployeeService employeeService = new EmployeeService();
        Employee emp10001 = employeeService.getEmployee(10001);
        System.out.println(emp10001);
    }

    static void test(EmployeeService employeeService) {
        List<Employee> employees = new ArrayList<>();

        long start = System.currentTimeMillis();
        for (int i = 0; i < 999; i++) {
            employees.add(employeeService.getEmployee(10001 + i));
        }
        long stop = System.currentTimeMillis();

        System.out.println(employees.size() + " in " + (stop - start) + "ms");
    }

//    static void getEmp() {
//        try (Connection connection = DriverManager.getConnection(URL, USER, PSW);
//             PreparedStatement statement = connection.prepareStatement(SQL)) {
//            statement.setInt(1, 10);
////            statement.setInt(2, 0);
//            ResultSet resultSet = statement.executeQuery();
//            while (resultSet.next()) {
//                System.out.println(resultSet.getString("first_name"));
//            }
//
//        } catch(SQLException e) {
//            System.out.println("klaida");
//            e.printStackTrace();
//        }
//    }
//
//    static void insertEmp(int empNo, LocalDate birthDate, String fName, String lName, String gender, LocalDate hireDate) {
//        try (Connection connection = DriverManager.getConnection(URL, USER, PSW);
//             PreparedStatement statement = connection.prepareStatement(InsertSql)) {
//            statement.setInt(1, empNo);
//            statement.setDate(2,  Date.valueOf(birthDate));
//            statement.setString(3, fName);
//            statement.setString(4, lName);
//            statement.setString(5, gender);
//            statement.setDate(6,  Date.valueOf(hireDate));
//            statement.executeUpdate();
//        } catch(SQLException e) {
//            System.out.println("klaida");
//            e.printStackTrace();
//        }
//    }
//
//    static void updateEmp(LocalDate birthDate, String fName, String lName, String gender, LocalDate hireDate, int empNo) {
//        try (Connection connection = DriverManager.getConnection(URL, USER, PSW);
//             PreparedStatement statement = connection.prepareStatement(UpdateSql)) {
//            statement.setDate(1,  Date.valueOf(birthDate));
//            statement.setString(2, fName);
//            statement.setString(3, lName);
//            statement.setString(4, gender);
//            statement.setDate(5,  Date.valueOf(hireDate));
//            statement.setInt(6, empNo);
//            statement.executeUpdate();
//        } catch(SQLException e) {
//            System.out.println("klaida");
//            e.printStackTrace();
//        }
//    }
//
//    static void removeEmp(int empNo) {
//        try (Connection connection = DriverManager.getConnection(URL, USER, PSW);
//             PreparedStatement statement = connection.prepareStatement(removeSql)) {
//            statement.setInt(1,  empNo);
//            statement.executeUpdate();
//        } catch(SQLException e) {
//            System.out.println("klaida");
//            e.printStackTrace();
//        }
//    }

}


